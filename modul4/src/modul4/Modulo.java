package modul4;

public class Modulo {

	public static void main(String[] args) {
		
		int zahl1 = 16;
		int zahl2 = 4;
		
		int ergebnis;
		
		ergebnis = zahl1 / zahl2; // Ergebnis: 3
		
		System.out.println("Ergebnis: " + ergebnis);
		
		ergebnis = zahl1 % zahl2; // Ergebnis: 3
		System.out.println("Ergebnis: " + ergebnis);
	}

}
