package modul4;

public class Vergleichsoperatoren {

	public static void main(String[] args) {
		
		boolean test = 1 != 2;
		boolean test1;
		boolean test2;
		boolean test3;
		
		test1 = 1 == 2;
		test2 = 1 == 1;
		test3 = 1 > 2;
		
		System.out.println(test);
		System.out.println(test1);
		System.out.println(test2);
		System.out.println(test3);
	}

}
