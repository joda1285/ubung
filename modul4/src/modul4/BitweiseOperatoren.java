package modul4;

public class BitweiseOperatoren {

	public static void main(String[] args) {

		// << linksshift, alle Bits nach links schieben
		// >> Rechtsshift, alle Bits nach rechts schieben
		// & UND-Operator; 2&3
		// | ODER-Operator; 2 | 3
		// ^ exklusiv-ODER-Operator 2 ^ 3
		
		int a = 6;
		int b = a & 0xFD;
		// Addition auf Bit-Ebene:
		//	0000 0110					Zahl 6
		//	1111 1101					Zahl 0xFD
		//____________					der UND-Operator sorgt daf�r, dass nur die 1en stehen bleiben, die bei beiden Zahlen gesetzt sind:
		//	0000 0100					Zahl 4
		
		int c = 6;
		int d = c | 0xFD;
		// Addition auf Bit-Ebene:
				//	0000 0110					Zahl 6
				//	1111 1101					Zahl 0xFD
				//____________					der ODER-Operator sorgt daf�r, dass alle 1en stehen bleiben, wo eine steht:
				//	1111 1111					Zahl 255
		
		int e = 6;
		int f = e ^ 0xFD;
		// Addition auf Bit-Ebene:
				//	0000 0110					Zahl 6
				//	1111 1101					Zahl 0xFD
				//____________					Exklusiv-Oder-Operator sorgt daf�r, dass alle 1en invertiert werden
				//	1111 1011					Zahl 255
		
		int g = 6;
		int h = g << 2;
		// Addition auf Bit-Ebene:
				//	0000 0110					Zahl 6
				//	1111 1101					Zahl 0xFD
				//____________					Exklusiv-Oder-Operator sorgt daf�r, dass alle 1en invertiert werden
				//	1111 1011					Zahl 255
		
		System.out.println(b);
		System.out.println(d);
		System.out.println(f);
		System.out.println("h: " + h);
		

	}

}
