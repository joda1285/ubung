package modul5;

import java.util.Scanner;

public class SwitchCaseAnweisung {

	public static void main(String[] args) {
		// 

		int auswahl = 0;
		System.out.println("Bitte geben Sie die Zahl 1, 2 oder 3 ein: ");
		Scanner sc = new Scanner (System.in);
		
		auswahl = sc.nextInt();
		
		switch(auswahl) {
				case 1: System.out.println("Die Zahl 1 wurde eingegeben"); break; //die eingegebenen Zahlen werden auf den "case" gemapped. "break" sorgt daf�r, dass damach damm schluss ist. ohne das "break" geht es danach weiter
				case 2: System.out.println("Die Zahl 2 wurde eingegeben"); break;
				case 3: case 5: { 
					System.out.print("Die Zahl 3 "); 
					System.out.println("oder die Zahl 5 wurde eingegeben");
					break;
				}
				default: System.out.println("Es wurde weder 1, 2 oder 3 eingegeben");
		}

	}

}
