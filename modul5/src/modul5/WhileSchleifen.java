package modul5;

public class WhileSchleifen {

	public static void main(String[] args) {
		//Kopfschleife
		int a;
		int b;
		
		a = 1;
		b = 10;
		 
		while(a < b) {												//Pr�fung der Bedingung
			System.out.println("Das ist eine Test-Ausgabe " + a);	//Anweisung
			++a;
		}

	}

}
