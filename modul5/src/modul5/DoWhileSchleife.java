package modul5;

public class DoWhileSchleife {

	public static void main(String[] args) {
		 
		//Fu�-Schleife
		
		int testVariable = 14;
		
		do {														//diese Anweisung wird immer mindestens einmal ausgef�hrt
			System.out.println("Ausgabe-Countdown "+testVariable); 
		}
		while(testVariable-- > 5);									//hier wird die Bedingung gepr�ft
		

	}

}
