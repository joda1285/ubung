package modul5;

public class Bedingungsoperator {

	public static void main(String[] args) {
		// 

		int a = 10;
		int b = 15;
		int max;
		
		/*
		
		if (a > b) {
			
			max = a;
		}
		else {
			max = b;
		}
		*/
		
		max = (a > b) ? b : a; // gleiche Bedeutung wie die if/else Verzweigung; ist die erste Anweisung (in Klammern) true, wird die erste Variable genommen, ansonsten die zweite

		System.out.println(max);
	}

}
