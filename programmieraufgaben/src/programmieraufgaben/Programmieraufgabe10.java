package programmieraufgaben;

import java.util.Scanner;

public class Programmieraufgabe10 {

	public static void main(String[] args) {
		

		float K�rpergewicht;
		float Gr��e;
		float ergebnis;
		String gruppe;
		
		Scanner sc = new Scanner (System.in);
		
		System.out.println("Bitte geben Sie ihr K�rpergewicht (in KG) ein: ");
		K�rpergewicht = sc.nextFloat();
		
		System.out.println("Bitte geben Sie ihre Gr��e (in Meter) ein: ");
		Gr��e = sc.nextFloat();
		
		ergebnis = K�rpergewicht / (Gr��e * Gr��e);
		ergebnis = Math.round(ergebnis);
		gruppe = einteilung(ergebnis); 
		
		System.out.println("Ihr BMI betr�gt " + ergebnis);
		System.out.println("Sie haben: "+gruppe);
	
	
	}
		
	public static String einteilung(float ergebnis) {
		if(ergebnis >= 18.5 && ergebnis <= 24) {
			return "Normalgewicht";
		}
		else if(ergebnis >= 25 && ergebnis <= 29) {
			return "�bergewicht";
		}
		else if(ergebnis >= 30 && ergebnis <= 34) {
			return "Adipositas Grad 1";
		}
		else if(ergebnis >= 35 && ergebnis <= 39) {
			return "Adipositas Grad 2";
		}
		else if(ergebnis >= 40) {
			return "Adipositas Grad 3";
		}
		else {
			return "Fehler";
		}
	}
	
}
	
	
	
	
	
	
	
	