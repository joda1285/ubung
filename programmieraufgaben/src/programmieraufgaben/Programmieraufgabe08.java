package programmieraufgaben;

public class Programmieraufgabe08 {

	public static void main(String[] args) {
		
//		Meine Lösung:
//		int i = 0;
//		
//		while(i <= 100) {
//			
//			if((i % 3) == 0) {
//				if((i % 4) == 0) {
//					System.out.println("teilbar durch 3 und 4");
//				}
//				else {
//					System.out.println("teilbar durch 3");
//				}
//			}
//			else if ((i % 4) == 0) {
//				System.out.println("teilbar durch 4");
//			}
//			
//			else {
//				System.out.println(i);	
//			}
//			
//			i++;
//			
//
//		}
		
//		Musterlösung: vom ....
		
		for(int a = 0; a < 100; a++) {
			
			if(((a+1) % 3 == 0 ) && ((a+1) % 4) == 0 ) {
				System.out.println("durch 3 und 4 teilbar");
			}
			
		else if(((a+1) % 3 == 0 )) {
				System.out.println("teilbar durch 3");
			}
			else if(((a+1) % 4) == 0 ) {
				System.out.println("teilbar durch 4");
			}
			else {
				System.out.println(a + 1);
			}
				
			
		}
		
	}

}
