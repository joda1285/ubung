package programmieraufgaben;

import java.util.Scanner;

public class Programmieraufgabe06 {

	public static void main(String[] args) {
	
		float K�rpergewicht;
		float Gr��e;
		float ergebnis;
		
		Scanner sc = new Scanner (System.in);
		
		System.out.println("Bitte geben Sie ihr K�rpergewicht (in KG) ein: ");
		K�rpergewicht = sc.nextFloat();
		
		System.out.println("Bitte geben Sie ihre Gr��e (in Meter) ein: ");
		Gr��e = sc.nextFloat();
		
		ergebnis = K�rpergewicht / (Gr��e * Gr��e);
		
		System.out.println("Ihr BMI betr�gt " + ergebnis);

	}

}
