package modul6;

import java.util.Scanner;

public class AddiererMethode {

	public static void main(String[] args) {
		
		int zahl1, zahl2, ergebnis;
		zahl1 = 0;
		zahl2 = 0;
		ergebnis = 0;
		
		ergebnis = addierer(zahl1, zahl2);
		System.out.println("Das Ergebnis ist: " + ergebnis);

	}
	
	
	
	public static int addierer(int z1, int z2) { // Die �bergabeparamter m�ssen in der gleichen Reihenfolge stehen wie in der main Methode, k�nnen aber anders hei�en
		
		Scanner sc = new Scanner (System.in);
		
		System.out.println("Das ist ein Programm zum addieren von zwei Ganzzahlen: ");
		System.out.println("Bitte geben Sie die erste Ganzzahl ein: ");
		
		z1 = sc.nextInt();
		
		System.out.println("Bitte geben Sie die zweite Ganzzahl ein: ");
		z2 = sc.nextInt();
		
		return (z1 + z2);
		
		
	}

}
